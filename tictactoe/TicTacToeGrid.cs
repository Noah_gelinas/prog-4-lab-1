namespace tictactoe{

    class TicTacToeGrid {
        

        
        public TicTacToeGrid() {
            Grid = new char[3,3];
            for(int i=0;i<Grid.GetLength(0);i++) {
                for(int j=0;j<Grid.GetLength(0);j++) {
                    Grid[i,j]=' ';
                }
            }
        }

        char [,] Grid {get;set;}
        public void PlaceCharacter(char character,int row, int col) {
            Grid[row,col]=character;
        }

        public void PrintGrid() {
            
            for(int i=0;i<Grid.GetLength(0);i++) {
                for(int j=0;j<Grid.GetLength(0);j++) {
                    Console.Write(Grid[i,j]);
                    if(j<2) {
                        Console.Write(" | ");
                    }
                }
                Console.WriteLine();
            }
        }

        public char CheckRows() {
            char curr;
            char winner=' ';
            for(int i=0;i<Grid.GetLength(0);i++) {
                curr=Grid[i,0];
                if(curr==Grid[i,1] && curr==Grid[i,2]) {
                    winner=Grid[i,0];
                }
            }
            return winner;
        }

        public char CheckCol() {
            char curr;
            char winner=' ';
            for(int i=0;i<Grid.GetLength(0);i++) {
                curr=Grid[0,i];
                if(curr==Grid[1,i] && curr==Grid[2,i]) {
                    winner=Grid[0,i];
                }
            }
            return winner;
        }

        public char CheckDiagonals() {
            char curr1;
            char curr2;
            char winner=' ';
            int i=0;
                curr1=Grid[i,0];
                curr2=Grid[i,2];
                if(curr1==Grid[i+1,1] && curr1==Grid[i+2,2]) {
                    winner=Grid[0,i];
                }else if(curr2==Grid[i+1,1] && curr2 == Grid[i+2,0]) {
                    winner=Grid[i,2];
                }
            
            return winner;
        }

        public string CheckGrid(TicTacToeGrid gridObj) {
            
            string winner=" ";
            winner = Char.ToString(gridObj.CheckRows());
            if (String.Equals(winner,"X") || String.Equals(winner,"O")) {
                return winner;       
            }else if(String.Equals(winner," ")) {
                winner = Char.ToString(gridObj.CheckCol());
                if (String.Equals(winner,"X") || String.Equals(winner,"O")) {
                    return winner;
                }
            }

            if(String.Equals(winner," ")) {     
                winner = Char.ToString(gridObj.CheckDiagonals());
                if(String.Equals(winner,"X") ||String.Equals(winner,"O")) {
                    return winner;
                }
            }
                for(int i=0;i<Grid.GetLength(0);i++) {
                    for (int j=0;j<Grid.GetLength(0);j++) {
                        if (Grid[i,j] == ' ') {
                            return winner;
                        }
                    }
                }
                return "tie";
        }
    }
}
