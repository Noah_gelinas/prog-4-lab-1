namespace tictactoe {

    class TicTacToeGame {
        public static void Main(String[] args) {
            bool valid=false;
            int col=0;
            int row=0;
            char character=' ';
            TicTacToeGrid gridObj= new TicTacToeGrid();
            
                Console.WriteLine("Enter a column number");
                col = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter a row number");
                row = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Enter Character to Place 'X' or 'O'");
                character = Convert.ToChar(Console.ReadLine());
                while(valid==false) {
                    if(character!='X' && character!='O') {
                        Console.WriteLine("Incorrect input! please only enter 'X' or 'O'");
                        character = Convert.ToChar(Console.ReadLine());
                    }else {
                        valid=true;
                    }
                }
                gridObj.PlaceCharacter(character,row,col);
            

                
                Console.WriteLine(gridObj.CheckRows());
                Console.WriteLine(gridObj.CheckCol());
                Console.WriteLine(gridObj.CheckDiagonals());
                gridObj.PrintGrid();
                Console.WriteLine(gridObj.CheckGrid(gridObj));
        }
    }
}